**Llama Platform**

Llama platform offers Merchants & Brand Ambassadors to create long term partnerships and collaborate on creating long term customers for Merchants. We've destilled most sought after functionalities and eliminated all the fluf to ensure that Llama is a 
seamleass conduit to growing revenues and commissions without any distractions.

[![Llama Demo](http://img.youtube.com/vi/OABszgs4kcA/0.jpg)](https://www.youtube.com/watch?v=OABszgs4kcA "Llama")


***WIKI***
https://bitbucket.org/c2mob/llama.app-public/wiki/

***Bug Tracker***
https://bitbucket.org/c2mob/llama.app-public/issues?status=new&status=open
---

